class CreateLinkContents < ActiveRecord::Migration
  def change
    create_table :link_contents do |t|
      t.string :name
      t.string :href

      t.timestamps null: false
    end
  end
end

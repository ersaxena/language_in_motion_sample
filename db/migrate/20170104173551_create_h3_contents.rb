class CreateH3Contents < ActiveRecord::Migration
  def change
    create_table :h3_contents do |t|
      t.integer :link_content_id
      t.text :h3_value

      t.timestamps null: false
    end
  end
end

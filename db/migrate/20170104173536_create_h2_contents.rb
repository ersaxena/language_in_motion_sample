class CreateH2Contents < ActiveRecord::Migration
  def change
    create_table :h2_contents do |t|
      t.integer :link_content_id
      t.text :h2_value

      t.timestamps null: false
    end
  end
end

module API
  module V1
    class Filters < Grape::API
      version 'v1'
      format :json
      
      helpers UserHelpers

      get "/website_content" do
        url_filter(params[:url])
      end

      get "/url_list" do
        LinkContent.all.map { |u| u.href }
      end
    end
  end
end
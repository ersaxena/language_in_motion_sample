require 'nokogiri'
require 'open-uri'

module UserHelpers
  extend Grape::API::Helpers
  
  def url_filter(url)
    # Fetch and parse HTML document
    doc = Nokogiri::HTML(open(url))
    @lc = LinkContent.find_by(href: url)

    if @lc.nil?
      @lc = LinkContent.create(:href => url)
    end

    doc.xpath('//h1', '//h2', '//h3','//a').each do |link|
      if link.name.eql?('a')
        LinkContent.create(:name => link.content.strip, :href => link['href'], parent_id: @lc.id)
      end  
      if link.name.eql?('h1')
        H1Content.create(:link_content_id => @lc.id, :h1_value => link.content.strip)
      end
      if link.name.eql?('h2')
        H2Content.create(:link_content_id => @lc.id, :h2_value => link.content.strip)
      end
      if link.name.eql?('h3')
        H3Content.create(:link_content_id => @lc.id, :h3_value => link.content.strip) 
      end
    end
  end
end


module API
  module V1
    class Root < Grape::API
      mount API::V1::Filters
      
      helpers UserHelpers
    end
  end
end